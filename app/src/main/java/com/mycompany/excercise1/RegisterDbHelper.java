package com.mycompany.excercise1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

public class RegisterDbHelper {

    public static abstract class DataBase implements BaseColumns {

        public static final String TABLE_NAME = "register";
        public static final String COLUMN_NAME_ENTRY_EMAIL = "email";
        public static final String COLUMN_NAME_ENTRY_NAME = "entry_name";
        public static final String COLUMN_NAME_PASS = "entry_pass";
        public static final String COLUMN_NAME_RETYPE = "repeat_pass";
    }
    private DbHelper mDbHelper;
    private SQLiteDatabase mDb;
    private final Context mCtx;
    private static final String TAG = "RegisterDb";

    private static class DbHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "Register.db";

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {


            String TEXT_TYPE = " TEXT";
            String COMMA = ",";
            String SQL_CREATE_ENTRIES =
                    "CREATE TABLE " + DataBase.TABLE_NAME + "(" +
                            DataBase._ID + "INTEGER PRIMARY KEY, " +
                            DataBase.COLUMN_NAME_ENTRY_EMAIL +
                            TEXT_TYPE + COMMA +
                            DataBase.COLUMN_NAME_ENTRY_NAME + TEXT_TYPE + COMMA +
                            DataBase.COLUMN_NAME_PASS + TEXT_TYPE + COMMA +
                            DataBase.COLUMN_NAME_RETYPE + TEXT_TYPE+ ")";

            db.execSQL(SQL_CREATE_ENTRIES);

            Log.i(TAG, "Database created"+ DataBase.TABLE_NAME);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String SQL_DELETE_ENTRIES =
                    "DROP TABLE IF EXISTS" + DataBase.TABLE_NAME;
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
            Log.i(TAG, "Table already exists, dropped and create other " + DataBase.TABLE_NAME);
        }

    }

    public RegisterDbHelper(Context ctx) {
        this.mCtx = ctx;
    }
    public RegisterDbHelper open() throws SQLException {
        mDbHelper = new DbHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        Log.i(TAG,"Database: " + mDbHelper.DATABASE_NAME + " Version: " + mDbHelper.DATABASE_VERSION + " Open");
        return this;

    }

    public void close() {
        mDbHelper.close();
    }

    public void createUser(String email, String name, String entry_pass, String retype) {
       //see change public long to void...
        ContentValues initialValues = new ContentValues();
        initialValues.put(DataBase.COLUMN_NAME_ENTRY_EMAIL, email);
        initialValues.put(DataBase.COLUMN_NAME_ENTRY_NAME, name);
        initialValues.put(DataBase.COLUMN_NAME_PASS, entry_pass);
        initialValues.put(DataBase.COLUMN_NAME_RETYPE, retype);

        mDb.insert(DataBase.TABLE_NAME, null, initialValues);
        Log.i(TAG, "User: " + email + ", " + name + ", " + entry_pass + ", " + retype + " Created in table: " + DataBase.TABLE_NAME);

    }
    public String getUser(String email){
        mDb = mDbHelper.getReadableDatabase();

        Cursor c=  mDb.query(DataBase.TABLE_NAME, null, DataBase.COLUMN_NAME_ENTRY_EMAIL + "=?", new String[]{email}, null, null, null);

        if(c.getCount()<1){
            c.close();
            return "DOES NOT EXIST";
        }
        c.moveToFirst();
        String password = c.getString(c.getColumnIndex("entry_pass"));

        c.close();

        Log.i(TAG, "Get User: " + email + password + " from table: " + DataBase.TABLE_NAME);
      return password;

    }
   public String getName(String email){
       Cursor c=  mDb.query(DataBase.TABLE_NAME, null, DataBase.COLUMN_NAME_ENTRY_EMAIL + "=?", new String[]{email}, null, null, null);
       if(c.getCount()<1){
           c.close();
           return "DOES NOT EXIST";
       }
       c.moveToFirst();
       String name = c.getString(c.getColumnIndex("entry_name"));

       c.close();

       Log.i(TAG, "Get UserName: " + name + " from table: " + DataBase.TABLE_NAME);
       return name;

   }
    public String getEmail(String email){
        Cursor c=  mDb.query(DataBase.TABLE_NAME, null, DataBase.COLUMN_NAME_ENTRY_EMAIL + "=?", new String[]{email}, null, null, null);
        if(c.getCount()<1){
            c.close();
            Log.i(TAG, "Email doesn't exist, create account");

            return "DOES NOT EXIST";
        }
        c.close();
        Log.i(TAG, "Find email: " + email + " from table: " + DataBase.TABLE_NAME + " ,Email found, do not create account");
        return email;
    }
    public void updatePass(String password, String newPassword){

        ContentValues changePass = new ContentValues();
        changePass.put(DataBase.COLUMN_NAME_PASS, newPassword);

        String selection = "entry_pass=?";
        String [] selectionArgs = {String.valueOf(password)};

        mDb.update(DataBase.TABLE_NAME, changePass, selection, selectionArgs);

        Log.i(TAG, "Update pass from table: " + DataBase.TABLE_NAME + " ");

    }
    public void deleteUserDb(String email){

        String selection = "email=?";
        String [] selectionArgs = {String.valueOf(email)};

        mDb.delete(DataBase.TABLE_NAME, selection,selectionArgs );

        Log.i(TAG, "Delete from table: " + DataBase.TABLE_NAME + " " + email);

    }

}
