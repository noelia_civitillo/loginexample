package com.mycompany.excercise1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import static com.mycompany.excercise1.R.id.text;
import static com.mycompany.excercise1.R.id.welcome;

public class WelcomeActivity extends AppCompatActivity {
    private static final String TAG = "WelcomeActivity";

    private RegisterDbHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();


               String userName = intent.getStringExtra(MainActivity.SEND_INFO);


        setContentView(R.layout.activity_welcome);

        TextView textView = (TextView) findViewById(R.id.welcome);
        textView.setText(getString(R.string.welcome) + "   " + userName);
    }

    public void resetPass(View view) {

        mDbHelper = new RegisterDbHelper(this);
        mDbHelper.open();

        final Dialog dialogSettings = new Dialog(WelcomeActivity.this);
        dialogSettings.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSettings.setContentView(R.layout.settings_dialog);

        final EditText editTextActualPass = (EditText) dialogSettings.findViewById(R.id.actual_pass);
        final EditText editTextNewPass = (EditText) dialogSettings.findViewById(R.id.new_pass);
        final EditText editTextRetypeNewPass = (EditText) dialogSettings.findViewById(R.id.retype_new_pass);
        final Button buttonSave = (Button) dialogSettings.findViewById(R.id.save_pass);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {


                String actualPass = editTextActualPass.getText().toString();
                String newPass = editTextNewPass.getText().toString();
                String retypePass = editTextRetypeNewPass.getText().toString();

                Intent intent = getIntent();
                String pass = intent.getStringExtra(MainActivity.SEND_PASS);

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toasts_layout, (ViewGroup) findViewById(R.id.toast_layout));
                layout.getBackground().setAlpha(225);

                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.AXIS_SPECIFIED, 0, 85);
                toast.setView(layout);

                if (actualPass.matches(".*\\s.*") || actualPass.equals("") || newPass.matches(".*\\s.*") || newPass.equals("") ||
                        retypePass.matches(".*\\s.*") || retypePass.equals("")) {
                    TextView text = (TextView) layout.findViewById(R.id.text_toast);
                    text.setText(getString(R.string.toast_complete_all_fields));
                    toast.show();
                } else {

                    if (!actualPass.equals(pass))

                    {
                        editTextActualPass.setBackgroundResource(R.drawable.edittext_error_custom);
                        editTextActualPass.getBackground();

                        editTextNewPass.setBackgroundResource(R.drawable.edittext_custom);
                        editTextNewPass.getBackground();

                        TextView text1 = (TextView) layout.findViewById(R.id.text_toast);
                        text1.setText(getString(R.string.toast_password_incorrect));
                        toast.show();
                    } else {
                        if (newPass.equals(actualPass)) {
                            editTextActualPass.setBackgroundResource(R.drawable.edittext_error_custom);
                            editTextActualPass.getBackground();

                            editTextNewPass.setBackgroundResource(R.drawable.edittext_error_custom);
                            editTextNewPass.getBackground();

                            TextView text2 = (TextView) layout.findViewById(R.id.text_toast);
                            text2.setText(getString(R.string.toast_new_password_equal));
                            toast.show();
                        } else {
                            if (newPass.length() < 4) {
                                editTextActualPass.setBackgroundResource(R.drawable.edittext_custom);
                                editTextActualPass.getBackground();

                                editTextNewPass.setBackgroundResource(R.drawable.edittext_error_custom);
                                editTextNewPass.getBackground();

                                TextView text2 = (TextView) layout.findViewById(R.id.text_toast);
                                text2.setText(getString(R.string.toast_more_charact));
                                toast.show();
                            } else {
                                if (!newPass.matches(".*\\d.*")) {

                                    editTextActualPass.setBackgroundResource(R.drawable.edittext_custom);
                                    editTextActualPass.getBackground();

                                    editTextNewPass.setBackgroundResource(R.drawable.edittext_error_custom);
                                    editTextNewPass.getBackground();

                                    TextView text2 = (TextView) layout.findViewById(R.id.text_toast);
                                    text2.setText(getString(R.string.toast_needs_number));
                                    toast.show();

                                } else {
                                    if (newPass.equals(retypePass)) {

                                        mDbHelper.updatePass(pass, newPass);
                                        Log.i(TAG, "Update pass: " + pass + "for " + newPass);

                                        editTextActualPass.setBackgroundResource(R.drawable.edittext_custom);
                                        editTextActualPass.getBackground();

                                        editTextNewPass.setBackgroundResource(R.drawable.edittext_custom);
                                        editTextNewPass.getBackground();

                                        TextView text3 = (TextView) layout.findViewById(R.id.text_toast);
                                        text3.setText(getString(R.string.toast_new_password_saved));
                                        toast.setGravity(Gravity.AXIS_SPECIFIED, 0,200);
                                        toast.show();

                                        dialogSettings.dismiss();
                                    } else {

                                        editTextActualPass.setBackgroundResource(R.drawable.edittext_custom);
                                        editTextActualPass.getBackground();

                                        editTextNewPass.setBackgroundResource(R.drawable.edittext_error_custom);
                                        editTextNewPass.getBackground();

                                        editTextRetypeNewPass.setBackgroundResource(R.drawable.edittext_error_custom);
                                        editTextRetypeNewPass.getBackground();

                                        TextView text4 = (TextView) layout.findViewById(R.id.text_toast);
                                        text4.setText(getString(R.string.toast_password_doesnt_match));
                                        toast.show();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        CheckBox checkBox = (CheckBox) dialogSettings.findViewById(R.id.show_pass);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChequed) {
                if (!isChequed) {
                    editTextActualPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editTextNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editTextRetypeNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    editTextActualPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editTextNewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editTextRetypeNewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });
        dialogSettings.show();

    }

    public void deleteUser(View view) {

        mDbHelper = new RegisterDbHelper(this);
        mDbHelper.open();

        final Dialog dialogDelete = new Dialog(this);
        dialogDelete.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDelete.setContentView(R.layout.delete_user_dialog);

        final Button buttonDelete = (Button) dialogDelete.findViewById(R.id.delete_button_yes);
        final Button buttonCancel = (Button) dialogDelete.findViewById(R.id.cancel_delete);


        buttonDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent intent = getIntent();
                String userEmail = intent.getStringExtra(MainActivity.SEND_EMAIL);
                mDbHelper.deleteUserDb(userEmail);

                final Dialog dialogBack = new Dialog(WelcomeActivity.this);
                dialogBack.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogBack.setContentView(R.layout.back_after_delete);
                dialogBack.setCancelable(false);

                final Button buttonBack = (Button) dialogBack.findViewById(R.id.button_back_to_main);

                buttonBack.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        WelcomeActivity.this.finish();

                    }

                });
                dialogBack.show();

            }

        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialogDelete.dismiss();
            }

        });
        dialogDelete.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
           dialogOnBackPress();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //TODO see this method

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dialogOnBackPress();

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void dialogOnBackPress() {

        final Dialog dialogSignOut = new Dialog(this);
        dialogSignOut.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSignOut.setContentView(R.layout.sign_out_dialog);

        final Button buttonSignOut = (Button) dialogSignOut.findViewById(R.id.sign_out);
        final Button buttonCancel = (Button) dialogSignOut.findViewById(R.id.cancel_sign_out);

        buttonSignOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WelcomeActivity.this.finish();

            }

        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialogSignOut.dismiss();
            }

        });
        dialogSignOut.show();
    }
    public void signOut(View view){
        dialogOnBackPress();
    }
}



