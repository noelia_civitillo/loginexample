package com.mycompany.excercise1;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Locale;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends Activity {

    Locale myLocale;

    private RegisterDbHelper mDbHelper;

    private static final String TAG = "MainActivity";

    public static final String SEND_INFO = "com.mycompany.excercise1.SEND_INFO";
    public static final String SEND_PASS = "com.mycompany.excercise1.SEND_PASS";
    public static final String SEND_EMAIL = "com.mycompany.excercise1.SEND_EMAIL";

    public void enterUser(View view) {
        mDbHelper = new RegisterDbHelper(this);
        mDbHelper.open();

        Intent intent = new Intent(this, WelcomeActivity.class);

        EditText editText = (EditText) findViewById(R.id.edit_email);
        EditText editText1 = (EditText) findViewById(R.id.pass);

        String userEmail = editText.getText().toString();
        String userPass = editText1.getText().toString();

        String userPassStored = mDbHelper.getUser(userEmail);

        Log.i(TAG, "The pass finded in db: " + userPassStored);
        Log.i(TAG, "The user enter in edit text is: " + userEmail);

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toasts_layout, (ViewGroup) findViewById(R.id.toast_layout));
        layout.getBackground().setAlpha(185);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.AXIS_SPECIFIED, 0, 75);
        toast.setView(layout);

            if (userEmail.matches(".*\\s.*") || userEmail.equals("")) {

                editText.setBackgroundResource(R.drawable.edittext_error_custom);
                editText.getBackground();
                editText1.setBackgroundResource(R.drawable.edittext_custom);
                editText1.getBackground();

                if (userPass.matches(".*\\s.*") || userPass.equals("")) {

                    editText1.setBackgroundResource(R.drawable.edittext_error_custom);
                    editText1.getBackground();
                }


                TextView text = (TextView) layout.findViewById(R.id.text_toast);
                text.setText(getString(R.string.toast_complete_all_fields));
                toast.show();


            } else {
                if (userPass.matches(".*\\s.*") || userPass.equals("")) {

                    editText.setBackgroundResource(R.drawable.edittext_custom);
                    editText.getBackground();

                    editText1.setBackgroundResource(R.drawable.edittext_error_custom);
                    editText1.getBackground();

                    TextView text = (TextView) layout.findViewById(R.id.text_toast);
                    text.setText(getString(R.string.toast_complete_all_fields));
                    toast.show();

                } else {
                    if (!userEmail.matches("^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$")) {

                        editText.setBackgroundResource(R.drawable.edittext_error_custom);
                        editText.getBackground();

                        editText1.setBackgroundResource(R.drawable.edittext_custom);
                        editText1.getBackground();

                        TextView text3 = (TextView) layout.findViewById(R.id.text_toast);
                        text3.setText(getString(R.string.toast_invalid_email));
                        toast.show();

                    } else {
                        if (userPass.equals(userPassStored)) {
                            String name = mDbHelper.getName(userEmail);
                            String email = mDbHelper.getEmail(userEmail);
                            intent.putExtra(SEND_INFO, name);
                            intent.putExtra(SEND_PASS, userPassStored);
                            intent.putExtra(SEND_EMAIL, email);

                            startActivity(intent);

                            editText.setBackgroundResource(R.drawable.edittext_custom);
                            editText.getBackground();
                            editText.setText("");

                            editText1.setBackgroundResource(R.drawable.edittext_custom);
                            editText1.getBackground();
                            editText1.setText("");

                            findViewById(R.id.edit_email);
                            findViewById(R.id.pass);

                        } else {
                            editText.setBackgroundResource(R.drawable.edittext_error_custom);
                            editText.getBackground();
                            editText1.setBackgroundResource(R.drawable.edittext_error_custom);
                            editText1.getBackground();
                            TextView text2 = (TextView) layout.findViewById(R.id.text_toast);
                            text2.setText(getString(R.string.toast_email_incorrect));
                            toast.show();
                        }
                    }
                }
            }
        }


    public void register(View view){
        Intent register= new Intent(this, RegisterActivity.class);
        startActivity(register);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Button english = (Button) findViewById(R.id.english);
        Button spanish = (Button) findViewById(R.id.spanish);

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocale("en");
            }
        });
        spanish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocale("es");
            }
        });


    }
    public void setLocale(String lang){

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration config = res.getConfiguration();
        config.locale = myLocale;
        res.updateConfiguration(config, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

}
