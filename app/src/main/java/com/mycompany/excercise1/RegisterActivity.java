package com.mycompany.excercise1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class RegisterActivity extends AppCompatActivity{

    //database: name mDbHelper
    private RegisterDbHelper mDbHelper;
    //Logs by tag:
    private static final String TAG = "RegisterActivity";
    //method insert onClick, button enter (activity_register.xml)

    public void insert(View view) {

        Log.i(TAG, "Insert User");

        // create the database in this activity and open
        mDbHelper = new RegisterDbHelper(this);
        mDbHelper.open();

        //obtain the views
        EditText editText = (EditText) findViewById(R.id.edit_mail);
        EditText editText1 = (EditText) findViewById(R.id.edit_name);
        EditText editText2 = (EditText) findViewById(R.id.enter_pass);
        EditText editText3 = (EditText) findViewById(R.id.retype_pass);

        //get text from edittext
        String editMail = editText.getText().toString();
        String editName = editText1.getText().toString();
        String editPass = editText2.getText().toString();
        String retypePass = editText3.getText().toString();


        String userDb = mDbHelper.getEmail(editMail);

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toasts_layout, (ViewGroup) findViewById(R.id.toast_layout));
        layout.getBackground().setAlpha(225);

        Toast toast= new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.AXIS_SPECIFIED,0,85);
        toast.setView(layout);

        if (editMail.matches(".*\\s.*")||editMail.equals("")|| editName.matches(".*\\s.*") || editName.equals("")||editPass.matches(".*\\s.*")|| editPass.equals("") || retypePass.matches(".*\\s.*")|| retypePass.equals("")) {
            TextView text = (TextView) layout.findViewById(R.id.text_toast);
            text.setText(getString(R.string.toast_complete_all_fields));
            toast.show();

        } else {
            if (!editMail.matches("^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$")) {

                editText.setBackgroundResource(R.drawable.edittext_error_custom);
                editText.getBackground();
                TextView text3 = (TextView) layout.findViewById(R.id.text_toast);
                text3.setText(getString(R.string.toast_invalid_email));
                toast.show();

            } else {
                if (editMail.equals(userDb)) {

                    editText.setBackgroundResource(R.drawable.edittext_error_custom);
                    editText.getBackground();

                    TextView text1 = (TextView) layout.findViewById(R.id.text_toast);
                    text1.setText(getString(R.string.toast_email_registered));
                    toast.show();

                } else {
                    if (editPass.length() < 4) {

                        editText.setBackgroundResource(R.drawable.edittext_custom);
                        editText.getBackground();

                        editText2.setBackgroundResource(R.drawable.edittext_error_custom);
                        editText2.getBackground();

                        TextView text1 = (TextView) layout.findViewById(R.id.text_toast);
                        text1.setText(getString(R.string.toast_more_charact));
                        toast.show();

                    } else {
                        if (!editPass.matches(".*\\d.*")) {

                            editText.setBackgroundResource(R.drawable.edittext_custom);
                            editText.getBackground();

                            editText2.setBackgroundResource(R.drawable.edittext_error_custom);
                            editText2.getBackground();

                            TextView text1 = (TextView) layout.findViewById(R.id.text_toast);
                            text1.setText(getString(R.string.toast_needs_number));
                            toast.show();
                        } else {
                            if (editPass.equals(retypePass)) {

                                editText.setBackgroundResource(R.drawable.edittext_custom);
                                editText.getBackground();

                                editText2.setBackgroundResource(R.drawable.edittext_custom);
                                editText2.getBackground();

                                editText3.setBackgroundResource(R.drawable.edittext_custom);
                                editText3.getBackground();

                                //call the method createUser from the db
                                mDbHelper.createUser(editMail, editName, editPass, retypePass);

                                final Dialog dialogBackSignIn = new Dialog(this);

                                dialogBackSignIn.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialogBackSignIn.setContentView(R.layout.backto_sign_in_dialog);
                                dialogBackSignIn.setCancelable(false);

                                final Button buttonBack = (Button) dialogBackSignIn.findViewById(R.id.sign_in_button_yes);

                                buttonBack.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View view) {
                                        RegisterActivity.this.finish();

                                    }

                                });
                                dialogBackSignIn.show();

                            } else {
                                editText.setBackgroundResource(R.drawable.edittext_custom);
                                editText.getBackground();

                                editText2.setBackgroundResource(R.drawable.edittext_error_custom);
                                editText2.getBackground();

                                editText3.setBackgroundResource(R.drawable.edittext_error_custom);
                                editText3.getBackground();

                                TextView text2 = (TextView) layout.findViewById(R.id.text_toast);
                                text2.setText(getString(R.string.toast_password_doesnt_match));
                                toast.show();
                            }
                        }


                    }
                }
            }
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTitle("");

        super.onCreate(savedInstanceState);

        Intent register = getIntent();

        setContentView(R.layout.activity_register);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

}
